//User clicks the button
$(document).ready(function(){
    //any btn clicked
    $('button').click(function(){
        //remove the previous click
        $('button').removeClass('selected');
        //select the clicked btn
        $(this).addClass('selected');

        var flickerAPI = "http://api.flickr.com/services/feeds/photos_public.gne?jsoncallback=?";
        //JS will get the word on that button
        var animal = $(this).text();
        var flickrOptions = {
            tags: animal,
            format: "json"
        };
        function displayPhotos(data) {
            //list of photos
            var photoHTML='<ul>';
           // $.each(array,function(i,item){
            $.each(data.items,function(i,photo){
                photoHTML+='<li class="grid-25 tablet-grid-50">';
                photoHTML+='<a href="'+ photo.link  +'" class="image">';
                photoHTML+='<img src="'+photo.media.m+'"></a></li>';
            });
            photoHTML+='</ul>';
            //push to html photo
            $('#photos').html(photoHTML);
        }

        //GET request to flickr
        $.getJSON(flickerAPI, flickrOptions, displayPhotos);
    });

});












//Recive JSON response



//Add link and image tag to the page



